<img align='right' src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="80">

### Hi there 👋, my name is Samandar
#### Backend and Development
![Backend and Development](https://media.licdn.com/dms/image/D4D16AQFaK_-uRU5wUw/profile-displaybackgroundimage-shrink_350_1400/0/1669816961474?e=1677110400&v=beta&t=ODj2UIr3PdSD4K8tSdMPMX6smbxNDmI56ZVkusNKWac)

I'm Samandar from Uzbekistan, and I do content on the Backend and Development. I really enjoy learning languages and frameworks like PHP and Laravel, as well as working in WordPress.

Skills: PHP / MySQL /  JS  / HTML / CSS / Linux

- 🔭 I’m currently working on Local company and Freelancer 
- 🌱 I’m currently learning Python, Go and Cyber Security 
- 📫 How to reach me: xkas2001@gmail.com 


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/xkas01)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg' alt='linkedin' height='40'>](https://www.linkedin.com/in/samandar-abdullayev-9b13891b7/)  

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=xkas01)](https://github.com/anuraghazra/github-readme-stats)

![GitHub stats](https://github-readme-stats.vercel.app/api?username=xkas01&show_icons=true)  

#### Coding Stats
<!--START_SECTION:waka-->

```text
Other            5 hrs 58 mins   ████████▒░░░░░░░░░░░░░░░░   33.16 %
```

<!--END_SECTION:waka-->

![GitHub metrics](https://metrics.lecoq.io/xkas01)  
